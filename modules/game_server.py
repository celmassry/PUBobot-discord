from modules import stats3, bot
from modules.exceptions import PubobotException
from modules import client
from json import loads, JSONDecodeError
from tabulate import tabulate


def get_server_pw(server_row):
    try:
        pw = loads(server_row[2])['pw']
    except JSONDecodeError:
        pw = ""
    except KeyError:
        pw = ""
    return pw


def server_rows_to_str(servers):

    def server_match(game_server_id):
        try:
            return next(match.id for match in bot.active_matches if match.server and match.server['game_server_id'] == game_server_id)
        except StopIteration:
            return '-'

    def format_ip_pw(ip, pw):
        if pw != "":
            return f"{ip};password {pw}"
        else:
            return ip

    return "```" + \
           tabulate(
               [
                   [
                    server[0],
                    format_ip_pw(server[1], get_server_pw(server)),
                    server_match(server[0])
                   ] for server in servers
               ],
               headers=['id', 'address&password', 'match'], tablefmt="presto") +\
           "```"


def process_command(channel, member, cmd, args, access_level, active_matches):
    try:
        if cmd == 'servers':
            client.notice(channel.channel, server_rows_to_str(stats3.get_game_servers(channel.guild.id)))
            return True

        elif cmd == 'add_server':
            return edit_command(access_level, args, channel, member)

        elif cmd == 'edit_server':
            try:
                return edit_command(access_level, args[1:], channel, member, game_server_id=args[1])
            except IndexError:
                raise PubobotException("usage:`!edit_server <id> de.game_server.com:1234 {\"pw\":\"secret\"}`")

        elif cmd == 'remove_server':
            if access_level > 1:
                server_id = channel.channel.guild.id
                try:
                    game_server_id = args[1]
                except IndexError:
                    raise PubobotException("usage:`!remove_server <id>`")
                stats3.remove_game_server(server_id, game_server_id)
                client.notice(channel.channel, "server removed")
            else:
                raise PubobotException("You have no right for this!")
            return True

        elif cmd == 'pick_server':
            pickup = None
            if len(args) > 1:
                pickup_name = args[1]
                for _pickup in channel.pickups:
                    if _pickup.name.lower() == pickup_name:
                        pickup = _pickup
                        break
                if pickup is None:
                    client.reply(channel.channel, member, "pickup not found")
                    return True

            picked_server = pick_server(channel, active_matches, pickup)
            pw = picked_server['pw'] if 'pw' in picked_server else ''
            client.notice(channel.channel,
                          "id: {} ip: {} password: {}".format(picked_server['game_server_id'], picked_server['ip'], pw))
            return True
    except PubobotException as e:
        client.notice(channel.channel, str(e))
        return True
    else:
        return False


def edit_command(access_level, args, channel, member, game_server_id=None):
    if access_level > 1:
        try:
            ip = args[1]
        except IndexError:
            if game_server_id is None:
                raise PubobotException("usage:`!add_server de.game_server.com:1234 {\"pw\":\"secret\"}`")
            else:
                raise IndexError
        server_id = channel.channel.guild.id
        try:
            additional_info = args[2]
            loads(additional_info)
        except JSONDecodeError as e:
            raise PubobotException("Additional info gotta be in json format `{\"pw\":\"secret\"}`: " + e.msg)
        except IndexError:
            additional_info = ""
            pass
        if game_server_id is None:
            stats3.add_game_server(server_id, ip, additional_info)
            client.notice(channel.channel, "server added")
        else:
            stats3.edit_game_server(game_server_id, server_id, ip, additional_info)
            client.notice(channel.channel, "server edited")
    else:
        raise PubobotException("You have no right for this!")
    return True


def dict_to_str(info):
    rv = info['ip']
    if info['pw'] != "":
        rv += " pw: " + info['pw']
    return rv


def to_dict(row):
    return {'game_server_id': row[0], 'ip': row[1], 'pw': get_server_pw(row), 'info': row[2]}


def get(game_server_id):
    return to_dict(stats3.get_game_server(game_server_id))


def pick_server(channel, active_matches, pickup=None):
    if pickup is None:
        config_ids = channel.cfg['servers']
    else:
        config_ids = channel.get_value('servers', pickup)
    servers = None
    if config_ids is None:
        servers = stats3.get_game_servers(channel.guild.id)
        ids = [server[0] for server in servers]
    else:
        try:
            ids = [int(server_id) for server_id in config_ids.split(',')]
        except ValueError:
            raise PubobotServerValueError

    if len(ids):
        for match in active_matches:
            if match.server is not None and match.channel.guild.id == channel.guild.id:
                try:
                    ids.remove(match.server['game_server_id'])
                except ValueError:
                    pass
    else:
        raise PubobotNoGameServersConfiguredException

    if len(ids):
        if servers is None:
            servers = stats3.get_game_servers(channel.guild.id)
        try:
            picked_server = next(server for server in servers if server['game_server_id'] == ids[0])
        except StopIteration:
            raise PubobotNoFreeServers
        return to_dict(picked_server)
    else:
        raise PubobotNoFreeServers


def validate_servers_value(value):
    ids = value.split(',')
    for server_id in ids:
        if not isinstance(int(server_id), int):
            raise ValueError


def set_servers_default(channel, member, variable, value):
    if value.lower() == 'none':
        channel.update_channel_config(variable, None)
        client.reply(channel.channel, member, "Removed {0} default value".format(variable))
    else:
        try:
            validate_servers_value(value)
        except ValueError:
            client.reply(channel.channel, member, "servers must be in format: `integer,integer,...` or `None`")
            return
        channel.update_channel_config(variable, value)
        client.reply(channel.channel, member, "Set '{0}' {1} as default value".format(value, variable))


def set_servers_pickups(channel, pickups, member, variable, value):
    if value.lower() == "none":
        for i in pickups:
            channel.update_pickup_config(i, variable, None)
        client.reply(channel.channel, member,
                     "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                    ", ".join(
                                                                                                        i.name
                                                                                                        for i in
                                                                                                        pickups)))
    else:
        try:
            validate_servers_value(value)
        except ValueError:
            client.reply(channel.channel, member, "servers must be in format: `integer,integer,...` or `None`")
            return
        for i in pickups:
            channel.update_pickup_config(i, variable, value)
        client.reply(channel.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
            i.name for i in pickups)))


class PubobotNoFreeServers(PubobotException):
    def __str__(self):
        return "No free server available"


class PubobotServerValueError(PubobotException):
    def __str__(self):
        return "servers value is wrongly configured"


class PubobotNoGameServersConfiguredException(PubobotException):
    def __str__(self):
        return "No game servers configured"
