import sqlite3
from datetime import timedelta
from time import time
from os.path import isfile
from decimal import Decimal
from typing import Optional, List, Tuple
from collections import defaultdict

from pypika import Query, Table, Order, Not, Parameter, Bracket

# INIT
version = 30


def init(db_file_name="database.sqlite3"):
    global conn, c, last_match
    db_exists = isfile(db_file_name)
    conn = sqlite3.connect(db_file_name)
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    if db_exists:
        check_db()
    else:
        print("DATATBASE| Creating new database...")
        create_tables()

    c.execute("SELECT pickup_id from pickups ORDER BY pickup_id DESC LIMIT 1")
    result = c.fetchone()
    if result:
        last_match = result[0]
    else:
        last_match = -1


def get_channels():
    c.execute("SELECT * from channels")
    channels = []
    for channel in c.fetchall():
        channels.append(dict(channel))

    for i in channels:
        i['channel_id'] = i['channel_id']
        i['server_id'] = i['server_id']

    return channels


def get_pickups(channel_id):
    c.execute("SELECT * from pickup_configs WHERE channel_id = ?", (channel_id,))
    pickups = c.fetchall()
    rv = []
    for pickup in pickups:
        rv.append(dict(pickup))
    return rv


def get_pickup_groups(channel_id):
    c.execute("SELECT group_name, pickup_names FROM pickup_groups WHERE channel_id = ?", (channel_id,))
    pg = c.fetchall()
    d = dict()
    for i in pg:
        d[i[0]] = i[1].split(" ")
    return d


def new_pickup_group(channel_id, group_name, pickup_names):
    c.execute("INSERT OR REPLACE INTO pickup_groups (channel_id, group_name, pickup_names) VALUES (?, ?, ?)",
              (channel_id, group_name, " ".join(pickup_names)))
    conn.commit()


def delete_pickup_group(channel_id, group_name):
    c.execute("DELETE FROM pickup_groups WHERE channel_id = ? AND group_name = ?", (channel_id, group_name))
    conn.commit()


def new_channel(server_id, server_name, channel_id, channel_name, admin_id):
    c.execute("INSERT OR REPLACE INTO channels (server_id, server_name, channel_id, channel_name, first_init, admin_id)"
              " VALUES (?, ?, ?, ?, ?, ?)",
              (server_id, server_name, channel_id, channel_name, str(int(time())), admin_id))
    conn.commit()
    c.execute("SELECT * from channels WHERE channel_id = ?", (channel_id,))
    chan = c.fetchone()
    return dict(chan)


def new_pickup(channel_id, pickup_name, max_players):
    c.execute("INSERT INTO pickup_configs (channel_id, pickup_name, maxplayers) VALUES (?, ?, ?)",
              (channel_id, pickup_name, max_players))
    conn.commit()
    c.execute("SELECT * from pickup_configs WHERE channel_id = ? AND pickup_name = ?", (channel_id, pickup_name))
    result = c.fetchone()
    return dict(result)


def delete_pickup(channel_id, pickup_name):
    c.execute("DELETE FROM pickup_configs "
              "WHERE channel_id = ? AND pickup_name = ? COLLATE NOCASE", (channel_id, pickup_name))
    conn.commit()


def delete_channel(channel_id):
    c.execute("DELETE FROM channels WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM channel_players WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM bans WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM pickup_configs WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM player_pickups WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM pickups WHERE channel_id = ?", (channel_id,))
    conn.commit()


def reset_stats(channel_id):
    c.execute("DELETE FROM pickups WHERE channel_id = ?", (channel_id,))
    c.execute("DELETE FROM player_pickups WHERE channel_id = ?", (channel_id,))
    conn.commit()


def undo_ranks(channel_id, match_id):
    c.execute("SELECT user_id, user_name, rank_change, is_winner, is_panzer, ifnull(streak_before,0) "
              "FROM player_pickups WHERE channel_id = ? AND pickup_id = ? AND is_ranked = 1",
              (channel_id, match_id))
    l = c.fetchall()
    if len(l):
        c.execute("UPDATE player_pickups SET is_ranked = 0 WHERE channel_id = ? AND pickup_id = ?",
                  (channel_id, match_id))
        for user_id, user_name, rank_change, is_winner, is_panzer, streak_before in l:
            if is_panzer:
                c.execute("UPDATE channel_players SET panzer_rank=panzer_rank-(?), pf_wins=pf_wins-?, pf_draws=pf_draws-?, pf_loses=pf_loses-?, streak = ?"
                          " WHERE channel_id = ? AND user_id = ?",
                          (rank_change, 1 if is_winner == 1 else 0, 1 if is_winner == 0.5 else 0, 1 if is_winner == 0 else 0, streak_before, channel_id, user_id))
            else:
                c.execute("UPDATE channel_players SET rank=rank-(?), wins=wins-?, draws=draws-?, loses=loses-?, streak = ?"
                          " WHERE channel_id = ? AND user_id = ?",
                          (rank_change, 1 if is_winner == 1 else 0, 1 if is_winner == 0.5 else 0, 1 if is_winner == 0 else 0, streak_before, channel_id, user_id))
        conn.commit()
        return "\n".join(["`{}` - **{:+}** points".format(i[1], 0 - i[2]) for i in l])
    else:
        return "No changes made."


def seed_player(channel, user_id, user_name, rating, defaultrating, pf=False):
    c.execute("SELECT user_id FROM channel_players WHERE channel_id = ? AND user_id = ?", (channel.id, user_id))
    if pf:
        if c.fetchone():
            c.execute("UPDATE channel_players SET panzer_rank = ?, is_seeded = ? WHERE channel_id = ? AND user_id = ?",
                      (rating, True, channel.id, user_id))
        else:
            c.execute("INSERT INTO channel_players (channel_id, user_id, nick, rank, wins, draws, loses, streak, is_seeded, panzer_rank, pf_wins, pf_draws, pf_loses) VALUES (?, ?, ?, ?, 0, 0, 0, 0, ?, ?, 0, 0, 0)",
                      (channel.id, user_id, user_name, defaultrating or 1400, True, rating))
    else:
        if c.fetchone():
            c.execute("UPDATE channel_players SET rank = ?, is_seeded = ? WHERE channel_id = ? AND user_id = ?",
                      (rating, True, channel.id, user_id))
        else:
            c.execute("INSERT INTO channel_players (channel_id, user_id, nick, rank, wins, draws, loses, streak, is_seeded, panzer_rank, pf_wins, pf_draws, pf_loses) VALUES (?, ?, ?, ?, 0, 0, 0, 0, ?, ?, 0, 0, 0)",
                      (channel.id, user_id, user_name, rating, True, defaultrating or 1400))
    conn.commit()


def reset_ranks(channel_id):
    c.execute("UPDATE channel_players SET rank = NULL, wins = NULL, draws = NULL, loses = NULL, streak = NULL, is_seeded = NULL, panzer_rank = NULL, pf_wins = NULL, pf_draws = NULL, pf_loses = NULL "
              "WHERE channel_id = ?", (channel_id,))
    conn.commit()


def register_pickup(match, at=None):
    new_ranks = dict()
    if at is None:
        at = int(time())

    # update ranks because manual report or undo_ranks could have happened
    match.ranks = get_ranks(match.pickup.channel, [i.id for i in match.players])

    players_str = " " + " ".join([i.nick or i.name for i in match.players]) + " "
    if match.alpha_team and match.beta_team:
        alpha_str = " ".join([i.nick or i.name for i in match.alpha_team])
        beta_str = " ".join([i.nick or i.name for i in match.beta_team])
    else:
        beta_str = None
        alpha_str = None

    maps = match.map if hasattr(match, 'map') else ""

    panzers = " ".join([i.nick or i.name for i in match.panzers]) if match.panzers else ""

    c.execute("INSERT INTO pickups "
              "(pickup_id, channel_id, pickup_name, at, players, alpha_players, beta_players, is_ranked, winner_team, maps, panzers) "
              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
              (match.id, match.pickup.channel.id, match.pickup.name, at,
               players_str, alpha_str, beta_str, match.ranked, match.winner, maps, panzers))

    if len(match.panzers): #use panzer elo for calculation if panzers were set
        for player in match.panzers:
            match.ranks[player.id] = match.panzer_ranks[player.id]

    if match.ranked and match.winner:
        alpha_rank = int(sum([match.ranks[player.id] for player in match.alpha_team]) / len(match.alpha_team))
        beta_rank = int(sum([match.ranks[player.id] for player in match.beta_team]) / len(match.beta_team))

        # [alpha, beta]
        expected_scores = [1 / (1 + 10 ** ((beta_rank - alpha_rank) / 400)),
                           1 / (1 + 10 ** ((alpha_rank - beta_rank) / 400))]
        if match.winner == 'alpha':
            scores = [1, 0, 0]
        elif match.winner == 'draw':
            scores = [0.5, 0.5, 1]
        else:
            scores = [0, 1, 0]
        draw = 2  # index of draw in scores

    for player in [player for player in match.players if player not in match.unpicked]:
        user_name = player.nick or player.name
        team = None
        is_lastpick = player == match.lastpick  # True or False
        is_panzer = player in match.panzers
        if match.alpha_team and match.beta_team:
            if player in match.alpha_team:
                team_num = 0
                team = 'alpha'
            elif player in match.beta_team:
                team_num = 1
                team = 'beta'

        if match.ranked and match.winner and team:
            c.execute("INSERT OR IGNORE INTO channel_players "
                      "(channel_id, user_id, nick, rank, wins, draws, loses, panzer_rank, pf_wins, pf_draws, pf_loses) VALUES (?, ?, ?, ?, 0, 0, 0, ?, 0, 0 ,0)",
                      (match.pickup.channel.id, player.id, user_name, match.ranks[player.id], match.ranks[player.id]))

            # if we need to calibrate this player add additional rank gain/loss boost
            rank_k = match.pickup.channel.cfg['ranked_multiplayer']
            if is_panzer:
                c.execute(
                    "SELECT ifnull(pf_wins,0), ifnull(pf_draws,0), ifnull(pf_loses,0), ifnull(streak,0), is_seeded FROM channel_players WHERE channel_id = ? AND user_id = ?",
                    (match.pickup.channel.id, player.id))
                result = c.fetchone()
            else:
                c.execute(
                    "SELECT ifnull(wins,0), ifnull(draws,0), ifnull(loses,0), ifnull(streak,0), is_seeded FROM channel_players WHERE channel_id = ? AND user_id = ?",
                    (match.pickup.channel.id, player.id))
                result = c.fetchone()
            wins, draws, loses, streak, is_seeded = [i or 0 for i in result]

            streak_before = streak
            is_ranked = True
            rank_change = int(rank_k * (scores[team_num] - expected_scores[team_num]))
            if match.pickup.channel.cfg['ranked_calibrate'] and wins + draws + loses < 8 and not is_seeded:
                rank_change = int(rank_change * ((10 - (wins + draws + loses)) / 2.0))

            if streak.__gt__(0) != scores[team_num].__gt__(0):
                streak = 0
            if scores[team_num] == 1:
                streak += 1
            elif scores[team_num] == 0.5:
                streak = 0
            else:
                streak -= 1
            if match.ranked_streaks == 1 and abs(streak) > 2:
                rank_change = int(rank_change * (min([abs(streak), 6]) / 2.0))
            elif match.ranked_streaks == 2 and abs(streak) > 2:
                rank_change = int(rank_change + streak)

            rank_after = match.ranks[player.id] + rank_change

            is_winner = scores[team_num]
            if is_panzer:
                c.execute(
                    "UPDATE channel_players SET nick = ?, panzer_rank = ?, pf_wins=?, pf_draws=?, pf_loses=?, streak=? "
                    "WHERE channel_id = ? AND user_id = ?",
                    (user_name, rank_after, wins if scores[draw] else wins + scores[team_num], draws + scores[draw],
                     loses if scores[draw] else loses + abs(scores[team_num] - 1),
                     streak, match.pickup.channel.id, player.id))
            else:
                c.execute("UPDATE channel_players SET nick = ?, rank = ?, wins=?, draws=?, loses=?, streak=? "
                          "WHERE channel_id = ? AND user_id = ?",
                          (user_name, rank_after, wins if scores[draw] else wins + scores[team_num],
                           draws + scores[draw], loses if scores[draw] else loses + abs(scores[team_num] - 1),
                           streak, match.pickup.channel.id, player.id))
            new_ranks[player.id] = [user_name, rank_after]

        else:
            is_ranked = False
            rank_change = None
            rank_after = None
            is_winner = None
            streak_before = None

        c.execute("INSERT OR IGNORE INTO player_pickups "
                  "(pickup_id, channel_id, user_id, user_name, pickup_name, at, team, is_ranked, is_winner, rank_after,"
                  " rank_change, is_lastpick, is_panzer, streak_before) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                  (match.id, match.pickup.channel.id, player.id, user_name, match.pickup.name, at, team, is_ranked,
                   is_winner, rank_after, rank_change, is_lastpick, is_panzer, streak_before))

    conn.commit()
    return new_ranks


def lastgame(channel_id, text=False):  # [id, gametype, ago, [players], [caps]]
    if not text:  # return lastest game
        c.execute("SELECT pickup_id, at, pickup_name, players, alpha_players, beta_players, winner_team, maps "
                  "FROM pickups WHERE channel_id = ? ORDER BY pickup_id DESC LIMIT 1", (channel_id,))
        result = c.fetchone()
    else:
        # try to find last game by gametype
        c.execute("SELECT pickup_id, at, pickup_name, players, alpha_players, beta_players, winner_team, maps FROM pickups "
                  "WHERE channel_id = ? and pickup_name = ? ORDER BY pickup_id DESC LIMIT 1 COLLATE NOCASE",
                  (channel_id, text))
        result = c.fetchone()
        if result is None:  # no results, try to find last game by player
            c.execute("SELECT pickup_id, at, pickup_name, players, alpha_players, beta_players, winner_team, maps "
                      "FROM pickups WHERE channel_id = ? and players LIKE ? "
                      "ORDER BY pickup_id DESC LIMIT 1", (channel_id, f"%{text}%"))
            result = c.fetchone()
    return result


def get_streaks(channel_id, size):
    week_ago = int(time()) - 604800
    c.execute(
        "SELECT streak, nick "
        "FROM channel_players "
        "WHERE channel_id = ? AND streak > 0 AND user_id IN "
        "(SELECT user_id FROM player_pickups WHERE channel_players.channel_id = player_pickups.channel_id AND at > ?) "
        "order by ifnull(streak,0) desc limit ?",
        (channel_id, week_ago, size))
    topstreaks = c.fetchall()
    c.execute(
        "SELECT -1*streak as streak, nick "
        "FROM channel_players "
        "WHERE channel_id = ? AND streak < 0 AND user_id IN "
        "(SELECT user_id FROM player_pickups WHERE channel_players.channel_id = player_pickups.channel_id AND at > ?) "
        "order by ifnull(streak,0) limit ?",
        (channel_id, week_ago, size))
    bottomstreaks = c.fetchall()
    return topstreaks, bottomstreaks

def get_ranks(channel, user_ids):
    d = dict()
    c.execute("SELECT user_id, rank FROM channel_players WHERE channel_id = ? AND user_id in ({seq})"
              .format(seq=','.join(['?'] * len(user_ids))), (channel.id, *user_ids))
    results = c.fetchall()
    for user_id, rank in results:
        if rank:
            d[user_id] = rank
    for user_id in user_ids:
        if user_id not in d.keys():
            d[user_id] = channel.cfg['initial_rating'] or 1400
    return d


def get_panzer_ranks(channel, user_ids):
    d = dict()
    c.execute("SELECT user_id, panzer_rank as rank FROM channel_players WHERE channel_id = ? AND user_id in ({seq})"
              .format(seq=','.join(['?'] * len(user_ids))), (channel.id, *user_ids))
    results = c.fetchall()
    for user_id, rank in results:
        if rank:
            d[user_id] = rank
    for user_id in user_ids:
        if user_id not in d.keys():
            d[user_id] = channel.cfg['initial_rating'] or 1400
    return d


def get_matches_count(channel, user_ids):
    d = dict()
    c.execute("SELECT user_id, wins+draws+loses+pf_wins+pf_draws+pf_loses FROM channel_players WHERE channel_id = ? AND user_id in ({seq})"
              .format(seq=','.join(['?'] * len(user_ids))), (channel.id, *user_ids))
    results = c.fetchall()
    for user_id, matches_count in results:
        if matches_count:
            d[user_id] = matches_count
    for user_id in user_ids:
        if user_id not in d.keys():
            d[user_id] = 0
    return d


def set_pref_maps(channel_id, user, prefs, defaultrating):
    c.execute("SELECT user_id FROM channel_players WHERE channel_id = ? AND user_id = ?", (channel_id, user.id))
    if c.fetchone():
        c.execute("UPDATE channel_players SET mappref = ? WHERE channel_id = ? AND user_id = ?",
                  (prefs, channel_id, user.id))
    else:
        c.execute("INSERT INTO channel_players (channel_id, user_id, nick, rank, wins, loses, streak, panzer_rank, pf_wins, pf_loses, mappref) VALUES (?, ?, ?, ?, 0, 0, 0, ?, 0, 0, ?)",
                  (channel_id, user.id, user.name, defaultrating or 1400, defaultrating or 1400, prefs))
    conn.commit()


def get_pref_maps(channel_id, user_id):
    c.execute("SELECT mappref FROM channel_players WHERE channel_id = ? AND user_id = ?", (channel_id, user_id))
    ret = c.fetchone()
    return ret['mappref'] if ret else ""


def get_last_maps(channel_id, name):
    c.execute("SELECT at, maps "
            "FROM pickups WHERE channel_id = ? and players LIKE ? "
            "ORDER BY pickup_id DESC LIMIT 1", (channel_id, f"%{name}%"))
    ret = c.fetchone()
    return ret['maps'] if ret else "", ret['at'] if ret else 0


def get_rank_details(channel_id, user_id=False, nick=False, win_loss_only=False):
    if win_loss_only:
        order = "wr"
    else:
        order = "rank"
    c.execute(
        "SELECT user_id, nick, case when ifnull(streak,0) < 0 then cast((-1)*streak as text)||? "
        "when ifnull(streak,0) > 0 then streak||? "
        "else ? end as streak, rank, ifnull(wins,0) as wins, ifnull(draws,0) as draws, ifnull(loses,0) as loses, "
        "ifnull((cast(wins as real)+0.5*cast(ifnull(draws,0) as real))/(wins+ifnull(draws,0)+loses),0) as wr, "
        "panzer_rank as pfrank, ifnull(pf_wins,0) as pfwins, ifnull(pf_draws,0) as pfdraws, ifnull(pf_loses,0) as pfloses, "
        "ifnull((cast(pf_wins as real)+0.5*cast(ifnull(pf_draws,0) as real))/(pf_wins+ifnull(pf_draws,0)+pf_loses),0) as pfwr "
        "FROM channel_players "
        f"WHERE channel_id = ? AND rank IS NOT NULL ORDER BY {order} DESC",
        ("L", "W", "-", channel_id))
    lb = c.fetchall()
    for i in lb:
        if (i['user_id'] == user_id) or (nick and i['nick'] and nick in i['nick'].lower()):
            c.execute(
                "SELECT pickup_id, at, pickup_name, rank_change, is_winner, ifnull(is_panzer,0) as is_panzer FROM player_pickups "
                "WHERE user_id = ? AND channel_id = ? AND is_ranked = 1 ORDER BY pickup_id DESC LIMIT 3",
                (i[0], channel_id))
            matches = c.fetchall()
            place = lb.index(i) + 1
            i = dict(i)
            i['place'] = place  # replace user_id with ladder position

            # add pf place
            i['pfplace'] = None
            if win_loss_only:
                order = "wr"
            else:
                order = "panzer_rank"
            c.execute(
                "SELECT user_id, nick, ? as filler " # filler has no functional purpose whatsoever, for some reason only passing channel_id as parameter produces a valueerror
                "FROM channel_players "
                f"WHERE channel_id = ? AND panzer_rank IS NOT NULL ORDER BY {order} DESC",
                ("rank", channel_id))
            pflb = c.fetchall()
            for j in pflb:
                if (j['user_id'] == user_id) or (nick and j['nick'] and nick in j['nick'].lower()):
                    i['pfplace'] = pflb.index(j) + 1
            return [i, matches]
    return [None, None]


def get_user_pickups(user_id, channel_id, index: int = 0):
    player_pickups = Table('player_pickups')
    pickups = Table('pickups')
    page_size = 10
    query = Query.from_(player_pickups)\
        .join(pickups).on_field("pickup_id")\
        .select(
        player_pickups.pickup_id,
        player_pickups.pickup_name,
        player_pickups.at,
        player_pickups.team,
        player_pickups.is_ranked,
        player_pickups.is_winner,
        player_pickups.rank_after,
        pickups.alpha_players,
        pickups.beta_players
    ) \
        .where(player_pickups.channel_id == Parameter('?')) \
        .where(player_pickups.user_id == Parameter('?')) \
        .orderby(player_pickups.pickup_id, order=Order.desc)
    query = query[index * page_size:page_size]
    c.execute(str(query), (channel_id, user_id))
    return [[column for column in row] for row in c.fetchall()]


def get_user_stats(user_id):
    channel_players = Table('channel_players')
    channels = Table('channels')
    query = Query.from_(channel_players).select(
        channel_players.channel_id,
        channels.channel_name,
        channels.server_name,
        channel_players.rank,
        channel_players.wins,
        channel_players.loses,
        channel_players.streak
    ).join(channels).using('channel_id').where(
        (channel_players.user_id == Parameter('?')) &
        Bracket(channels.hide_ranks.isnull() | channels.hide_ranks == 0)) \
        .orderby(channel_players.wins + channel_players.loses, order=Order.desc)
    c.execute(str(query), (user_id, ))
    channel_stats = c.fetchall()

    # for channel in channel_stats

    def get_pickups_dict(row):
        rv = {column_name: row[column_name] for column_name in row.keys()}
        rv['pickups'] = get_user_pickups(user_id, row['channel_id'])
        return rv

    return [get_pickups_dict(row) for row in channel_stats]


def get_ladder(channel_id, page, page_size=10, win_loss_only=False, pf=False):
    if not pf:
        period_limit = int(time()) - 7 * 24 * 60 * 60
        wins_col = 'wins'
        draws_col = 'draws'
        loses_col = 'loses'
        rank_col = 'rank'
    else:
        period_limit = int(time()) - 14 * 24 * 60 * 60
        wins_col = 'pf_wins'
        draws_col = 'pf_draws'
        loses_col = 'pf_loses'
        rank_col = 'panzer_rank'

    sql = (
        f"SELECT {rank_col} as rank, nick, ifnull({wins_col},0) as wins, ifnull({draws_col},0) as draws, ifnull({loses_col},0) as loses, "
        f"ifnull((cast({wins_col} as real)+0.5*cast(ifnull({draws_col},0) as real))/({wins_col}+ifnull({draws_col},0)+{loses_col}),0) as wr "
        "FROM channel_players "
    )

    if not win_loss_only:
        sql += f"WHERE channel_id = ? AND {rank_col} IS NOT NULL AND "
    else:
        sql += f"WHERE channel_id = ? AND {wins_col}+{draws_col}+{loses_col} > 0 AND "

    if pf:
        _w = "AND is_panzer = 1 "
    else:
        _w = ""
    sql += (
        "user_id IN "
        "(SELECT user_id FROM player_pickups WHERE channel_players.channel_id = player_pickups.channel_id "
        f"{_w}"
        "AND at > ?) "
    )
    if not win_loss_only:
        sql += "ORDER BY rank desc LIMIT ?"
    else:
        sql += "ORDER BY wr desc LIMIT ?"

    c.execute(sql, (channel_id, period_limit, (page + 1) * page_size))
    return c.fetchall()[page * page_size:]


def stats(channel_id, text=False):
    if not text:  # return overall stats
        c.execute("SELECT pickup_name, count(pickup_name) FROM pickups WHERE channel_id = ? GROUP BY pickup_name",
                  (channel_id,))
        l = c.fetchall()
        if l is not None:
            pickups = []
            total = 0
            for i in l:
                pickups.append("{0}: {1}".format(i[0], i[1]))
                total += i[1]
            return "Total pickups: {0} | {1}".format(total, ", ".join(pickups))
        else:
            return "No pickups played yet."
    else:
        # get total pickups count
        c.execute("SELECT count(channel_id) FROM pickups WHERE channel_id = ? GROUP BY channel_id", (channel_id,))
        l = c.fetchone()
        if l is not None:
            total = l[0]
        else:
            return "No pickups played yet."

        # try to find by pickup_name
        c.execute(
            "SELECT pickup_name, count(pickup_name) FROM pickups "
            "WHERE channel_id = ? AND pickup_name = ? COLLATE NOCASE GROUP BY pickup_name ",
            (channel_id, text))
        l = c.fetchone()
        if l is not None:
            percent = int((float(l[1]) / total) * 100)
            return "Stats for **{0}**. Played: {1} ({2}%).".format(l[0], l[1], percent)
        else:
            # try to find by user_name
            c.execute(
                "SELECT user_id, user_name FROM player_pickups "
                "WHERE channel_id = ? AND user_name = ? COLLATE NOCASE ORDER BY rowid DESC LIMIT 1",
                (channel_id, text))
            l = c.fetchone()
            if l is not None:
                user_id = l[0]
                user_name = l[1]
            else:
                return "Nothing found."

            c.execute(
                "SELECT pickup_name, count(pickup_name) FROM player_pickups "
                "WHERE channel_id = ? AND user_id = ? GROUP BY pickup_name",
                (channel_id, user_id))
            l = c.fetchall()
            pickups = []
            user_total = 0
            for i in l:
                pickups.append("{0}: {1}".format(i[0], i[1]))
                user_total += i[1]
            percent = int((float(user_total) / total) * 100)
            return "Stats for **{0}**. Played {1} ({2}%): {3}".format(user_name, user_total, percent,
                                                                      ", ".join(pickups))


def top(channel_id, time_gap=False, pickup=False, limit=10):
    if time_gap and pickup:
        c.execute(
            "SELECT user_name, count(user_id) FROM player_pickups "
            "WHERE channel_id = ? and pickup_name = ? and at > ? "
            "GROUP BY user_id ORDER by count(user_id) DESC LIMIT ?",
            (channel_id, pickup, time_gap, limit))
    elif time_gap:
        c.execute(
            "SELECT user_name, count(user_id) FROM player_pickups "
            "WHERE channel_id = ? and at > ? "
            "GROUP BY user_id ORDER by count(user_id) DESC LIMIT ?",
            (channel_id, time_gap, limit))
    elif pickup:
        c.execute(
            "SELECT user_name, count(user_id) FROM player_pickups "
            "WHERE channel_id = ? and pickup_name = ? "
            "GROUP BY user_id ORDER by count(user_id) DESC LIMIT ?",
            (channel_id, pickup, limit))
    else:
        c.execute(
            "SELECT user_name, count(user_id) FROM player_pickups "
            "WHERE channel_id = ? GROUP BY user_id ORDER by count(user_id) DESC LIMIT ?",
            (channel_id, limit))

    return c.fetchall()


def activity(channel_id, time_gap=False, pickup=False, back=0):
    number_of_lines = 12
    output = []

    for i in range(number_of_lines):
        if time_gap and pickup:
            c.execute(
                "SELECT count(*) as num_games FROM pickups "
                "WHERE channel_id = ? and pickup_name = ? and at between ? and ?",
                (channel_id, pickup, int(time()) - (back * number_of_lines + 1 + i) * time_gap,
                 int(time()) - (back * number_of_lines + i) * time_gap))
        elif time_gap:
            c.execute(
                "SELECT count(*) as num_games FROM pickups "
                "WHERE channel_id = ? and at between ? and ?",
                (channel_id, int(time()) - (back * number_of_lines + 1 + i) * time_gap,
                 int(time()) - (back * number_of_lines + i) * time_gap))
        out = c.fetchall()
        output.append((out[0]["num_games"], int(time()) - (back * number_of_lines + 1 + i) * time_gap,
                       int(time()) - (back * number_of_lines + i) * time_gap))
    return output


def top_maps(channel_id, time_gap=False, pickup=False, back=0):
    map_dict = defaultdict(int)
    if time_gap and pickup:
        c.execute(
            "SELECT maps FROM pickups "
            "WHERE channel_id = ? and pickup_name = ? and at between ? and ?",
            (channel_id, pickup, int(time()) - (back+1) * time_gap, int(time()) - back * time_gap))
    elif time_gap:
        c.execute(
            "SELECT maps FROM pickups "
            "WHERE channel_id = ? and at between ? and ?",
            (channel_id, int(time()) - (back+1) * time_gap, int(time()) - back * time_gap))
    elif pickup:
        c.execute(
            "SELECT maps FROM pickups "
            "WHERE channel_id = ? and pickup_name = ?",
            (channel_id, pickup))
    else:
        c.execute(
            "SELECT maps FROM pickups "
            "WHERE channel_id = ?",
            (channel_id,))

    maps = c.fetchall()
    for entry in maps:
        if entry["maps"]:
            for _map in entry["maps"].split(','):
                map_dict[_map.strip()] += 1
    return map_dict


def format_top(top_list):
    return ', '.join(["{0}: {1}".format(i[0], i[1]) for i in top_list])


def noadd(channel_id, user_id, user_name, duration, author_name, reason=''):
    c.execute("SELECT * FROM bans WHERE user_id = ? AND channel_id = ? AND active = 1", (user_id, channel_id))
    ban = c.fetchone()
    if ban is not None:
        c.execute("UPDATE bans SET at=?, duratation=?, author_name=?, reason=? "
                  "WHERE user_id = ? AND channel_id = ? AND active = 1",
                  (int(time()), duration, author_name, reason, user_id, channel_id))
        conn.commit()
        if duration >= 0:
            return "Updated {}'s noadd to {} from now.".format(user_name, str(timedelta(seconds=duration))), ""
        else:
            return "Updated {}'s noadd to permanent noadd.".format(user_name), ""
    else:
        # add new ban
        c.execute("INSERT INTO bans (channel_id, user_id, user_name, active, at, duratation, reason, author_name)"
                  " VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                  (channel_id, user_id, user_name, 1, int(time()), duration, reason, author_name))
        conn.commit()
        # Get a quote!
        c.execute("SELECT * FROM nukem_quotes ORDER BY RANDOM() LIMIT 1")
        quote = c.fetchone()
        if duration >= 0:
            return "Banned {} for {}.".format(user_name, str(timedelta(seconds=duration))), quote[0]
        else:
            return "Permanently banned {}.".format(user_name), quote[0]


def forgive(channel_id, user_id, user_name, unban_author_name):
    c.execute("SELECT * FROM bans WHERE user_id = ? AND channel_id = ? AND active = 1", (user_id, channel_id))
    ban = c.fetchone()
    if ban is not None:
        c.execute("UPDATE bans SET active = 0, unban_author_name = ?"
                  " WHERE user_id = ? AND channel_id = ? AND active = 1",
                  (unban_author_name, user_id, channel_id))
        conn.commit()
        return "{} forgiven.".format(user_name)
    return "Ban not found!"


TEMP_ONLY = 0
PERM_ONLY = 1
PERM_BOTH = 2


def noadds(channel_id: int,
           index: Optional[int] = None,
           user_id: Optional[int] = None,
           permanent: int = PERM_BOTH
           ) -> List[str]:
    page_size = 10
    params = [channel_id]
    bans = Table('bans')
    query = Query.from_(bans).select(
        bans.user_name,
        bans.active,
        bans.at,
        bans.duratation,
        bans.reason,
        bans.author_name,
        bans.unban_author_name
    ).where(bans.channel_id == Parameter('?'))
    if user_id is not None:
        query = query.where(bans.user_id == Parameter('?'))
        params.append(user_id)
    if permanent == PERM_ONLY:
        query = query.where(Not((bans.duratation > 0) & (bans.duratation < 60 * 60 * 24 * 30 * 3)))
    elif permanent == TEMP_ONLY:
        query = query.where((bans.duratation > 0) & (bans.duratation < 60 * 60 * 24 * 30 * 3))

    # query.where(
    #     Field('duratation') + Field('at') - time() > 0
    # )

    query = query.orderby(bans.at, order=Order.desc)

    if index is None:
        query = query.where(bans.active == 1)
    else:
        query = query[index * page_size:page_size]

    c.execute(str(query), params)
    bans = c.fetchall()
    bans_str = []
    for ban in bans:
        ban_active = ban[1] == 1
        ban_active_str = ":green_circle:" if ban_active else ":red_circle:"
        unbanned_by = ban[6]
        seconds_left = int(ban[3] - (time() - ban[2]))
        duration = timedelta(seconds=ban[3])
        if seconds_left < 0 and ban['duratation'] >= 0:
            if index is None:
                continue
            else:
                unbanned_by = "time"
                ban_active = False
                ban_active_str = ":red_circle:"
        if ban_active and ban['duratation'] >= 0:
            # TODO: use https://pypi.org/project/humanize/ for better timedelta str
            time_left = timedelta(seconds=seconds_left)
            # user_name, time_left, author: reason
            bans_str.append("{} {}, [for {}], {} left, by {}: {}".format(
                ban_active_str, ban[0], duration, time_left, ban[5], ban[4]))
        elif ban['duratation'] < 0:
            # permanent ban
            ago = timedelta(seconds=int(time() - ban[2]))
            # user_name, ago, author (reason)
            bans_str.append("{} {}, {} ago, by {} ({})".format(
                ban_active_str, ban[0], ago, ban[5], ban[4]))
        else:
            ago = timedelta(seconds=int(time() - ban[2]))
            # user_name, ago, author (reason), unban_author
            bans_str.append(
                "{} {}, [for {}], {} ago, by {} ({}), unbanned by {}".format(
                    ban_active_str, ban[0], duration, ago, ban[5], ban[4], unbanned_by))
    return bans_str


def check_memberid(channel_id, user_id) -> Tuple[bool, str, Optional[int]]:
    """
    check on bans and phrases

    returns (bool is_banned, string phrase, int default_expire)
    """

    c.execute("SELECT at, duratation, reason FROM bans WHERE user_id = ? AND channel_id = ? AND active = 1",
              (user_id, channel_id))
    ban = c.fetchone()
    if ban:
        ban = list(ban)
        seconds_left = int(ban[1] - (time() - ban[0]))
        if seconds_left > 0:
            time_left = timedelta(seconds=seconds_left)
            if ban[2] != '':
                ban[2] = " Reason : {0}".format(ban[2])
            return True, "You have been banned. {0} time left.{1}".format(time_left, ban[2]), None
        else:
            c.execute(
                "UPDATE bans SET active = 0, unban_author_name = ? WHERE user_id = ? AND channel_id = ? AND active = 1",
                ("time", user_id, channel_id))
            conn.commit()
            return False, "Be nice next time, please.", None

    # no bans, find phrases!
    c.execute("SELECT default_expire FROM players WHERE user_id = ?", (user_id,))
    l = c.fetchone()
    if l:
        expire = l[0]
    else:
        expire = None
    c.execute("SELECT phrase FROM channel_players WHERE user_id = ? AND channel_id = ?", (user_id, channel_id))
    l = c.fetchone()
    if l:
        phrase = l[0]
    else:
        phrase = None

    return False, phrase, expire


# get default user !expire time
def get_expire(user_id):
    c.execute("SELECT default_expire FROM players WHERE user_id = ?", (user_id,))
    l = c.fetchone()
    if l:
        return l[0]
    else:
        return None


# set default user !expire time
def set_expire(user_id, seconds):
    # create user if not exists
    c.execute("INSERT OR IGNORE INTO players (user_id) VALUES (?)", (user_id,))
    c.execute("UPDATE players SET default_expire = ? WHERE user_id = ?", (seconds, user_id))
    conn.commit()


def set_phrase(channel_id, user_id, phrase):
    # create user if not exists
    c.execute("INSERT OR IGNORE INTO channel_players (channel_id, user_id) VALUES (?, ?)", (channel_id, user_id))
    c.execute("UPDATE channel_players SET phrase = ? WHERE user_id = ? AND channel_id = ?",
              (phrase, user_id, channel_id))
    conn.commit()


def get_game_server(game_server_id):
    c.execute("SELECT game_server_id, ip, additional_info FROM game_servers WHERE game_server_id = ?",
              (game_server_id,))
    return c.fetchone()


def get_game_servers(server_id):
    c.execute("SELECT game_server_id, ip, additional_info FROM game_servers WHERE server_id = ? OR server_id IS NULL",
              (server_id,))
    return c.fetchall()


def add_game_server(server_id, ip, additional_info):
    c.execute("INSERT INTO game_servers (server_id, ip, additional_info) VALUES (?, ?, ?)",
              (server_id, ip, additional_info))
    conn.commit()


def edit_game_server(game_server_id, server_id, ip, additional_info):
    c.execute("UPDATE game_servers SET ip=?, additional_info=? WHERE server_id=? AND game_server_id=?",
              (ip, additional_info, server_id, game_server_id))
    conn.commit()


def remove_game_server(server_id, game_server_id):
    c.execute("DELETE FROM game_servers WHERE server_id = ? AND game_server_id = ?", (server_id, game_server_id))
    conn.commit()


def get_member_info(user_id, server_id):
    c.execute("SELECT * FROM member_info WHERE server_id = ? AND user_id = ?", (server_id, user_id))
    return c.fetchone()


def list_member_info(server_id, page=0):
    per_page = 20
    offset = page * per_page
    c.execute("SELECT * FROM member_info WHERE server_id = ? LIMIT ?, ?",
              (server_id, offset, per_page))
    return c.fetchall()


def set_member_info(user_id, server_id, nick, country):
    if get_member_info(user_id, server_id):
        c.execute("UPDATE member_info SET nick = ?, country = ? WHERE user_id = ? AND server_id = ?",
                  (nick, country, user_id, server_id))
    else:
        c.execute("INSERT OR IGNORE INTO member_info (user_id, server_id, nick, country) VALUES (?, ?, ?, ?)",
                  (user_id, server_id, nick, country))
    conn.commit()


def get_status_messages():
    c.execute("SELECT message_id, channel_id, channel_ids FROM status_messages")
    return c.fetchall()


def add_status_message(message_id: int, channel_id: int, channel_ids: list):
    c.execute("INSERT INTO status_messages (message_id, channel_id, channel_ids) VALUES (?, ?, ?)",
              (message_id, channel_id, ','.join([str(channel_id) for channel_id in channel_ids])))
    conn.commit()


def remove_status_message(message_id: int):
    c.execute("DELETE FROM status_messages WHERE message_id = ?", (message_id, ))
    conn.commit()


def update_channel_config(channel_id, variable, value):
    c.execute("UPDATE OR IGNORE channels SET \"{0}\" = ? WHERE channel_id = ?".format(variable), (value, channel_id))
    conn.commit()


def update_pickup_config(channel_id, pickup_name, variable, value):
    c.execute(
        "UPDATE OR IGNORE pickup_configs SET \"{0}\" = ? WHERE channel_id = ? and pickup_name = ?".format(variable),
        (value, channel_id, pickup_name))
    conn.commit()


def check_db():
    c.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = [i[0] for i in c.fetchall()]

    if "utility" not in tables:
        c.execute("""CREATE TABLE `utility`
            ( `variable` TEXT,
            `value` TEXT,
            PRIMARY KEY(`variable`) )""")

    c.execute("SELECT value FROM utility WHERE variable='version'")
    db_version = c.fetchone()
    if db_version:
        db_version = Decimal(db_version[0])
    else:
        db_version = -1

    if db_version < version:
        print("DATABASE| Updating database from '{0}' to '{1}'...".format(db_version, version))
        if db_version < 2:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `allow_offline` INTEGER DEFAULT 0
            """)
        if db_version < 3:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `promotemsg` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `promotemsg` TEXT
            """)

        if db_version < 4:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `ranked_multiplayer` INTEGER DEFAULT 32;
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `ranked_calibrate` INTEGER DEFAULT 1;
            """)

            c.execute("""ALTER TABLE `pickups`
            ADD COLUMN `is_ranked` BOOL""")

            c.execute("""ALTER TABLE `player_pickups`
            ADD COLUMN `is_ranked` BOOL""")
            c.execute("""ALTER TABLE `player_pickups`
            ADD COLUMN `rank_after` INTEGER""")
            c.execute("""ALTER TABLE `player_pickups`
            ADD COLUMN `rank_change` INTEGER""")

            # rename points to rank and add wins and loses counters
            c.executescript("""ALTER TABLE channel_players RENAME TO tmp_channel_players;
            CREATE TABLE channel_players(`channel_id` TEXT, `user_id` TEXT, `nick` TEXT, `rank` INTEGER, `wins` INTEGER, `loses` INTEGER, `phrase` TEXT, PRIMARY KEY(`channel_id`, `user_id`));
            INSERT INTO channel_players(channel_id, user_id, phrase) SELECT channel_id, user_id, phrase FROM tmp_channel_players;
            DROP TABLE tmp_channel_players""")

        if db_version < 5:
            # got to change all the ID's to INTEGER from TEXT to migrate to discord.py 1.0+
            if db_version < 4:
                c.execute("INSERT OR REPLACE INTO utility (variable, value) VALUES ('version', ?)", (str(version),))
                conn.commit()

            raise (Exception(
                "In order to migrate to discord.py 1.0+ database tables must be rebuilded. Please backup your database (database.sqlite3 file) and run updater.py."))

        if db_version < 6:
            # add custom team emojis
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `team_emojis` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `team_emojis` TEXT
            """)

        if db_version < 7:
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `streak` INTEGER
            """)
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `is_seeded` BLOB
            """)

        if db_version < 8:
            # add custom team names
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `team_names` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `team_names` TEXT
            """)

        if db_version < 9:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `global_expire` INTEGER
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `ranked_streaks` INTEGER DEFAULT 1
            """)

        if db_version < 10:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `match_livetime` INTEGER
            """)

        if db_version < 11:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `initial_rating` INTEGER
            """)

        if db_version < 12:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `party` INTEGER
            """)

        if db_version < 13:
            # add custom team names
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `map_pick_order` TEXT
            """)
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `best_of` INTEGER DEFAULT 1
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `best_of` INTEGER DEFAULT 1
            """)

        if db_version < 14:
            create_game_servers_table()

        if db_version < 15:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `servers` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `servers` TEXT
            """)

        if db_version < 16:
            create_member_info_table()

        if db_version < 17:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `hide_ranks` INTEGER
            """)

        if db_version < 18:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `whitelist_msg` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `whitelist_msg` TEXT
            """)

        if db_version < 19:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `public` INTEGER
            """)
            create_status_messages_table()

        if db_version < 20:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `panzer_role` INTEGER
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `panzer_role` INTEGER
            """)

        if db_version < 21:
            c.execute("""ALTER TABLE `pickups`
            ADD COLUMN `maps` TEXT
            """)

        if db_version < 22:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `map_probabilities` TEXT
            """)
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `no_repeat_maps` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `custom_ranks` TEXT
            """)

        if db_version < 23:
            c.execute("""ALTER TABLE `player_pickups`
            ADD COLUMN `is_panzer` BLOB
            """)
            c.execute("""ALTER TABLE `pickups`
            ADD COLUMN `panzers` TEXT
            """)
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `panzer_rank` INTEGER
            """)
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `pf_wins` INTEGER
            """)
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `pf_loses` INTEGER
            """)

        if db_version < 24:
            c.execute("""ALTER TABLE `player_pickups`
            ADD COLUMN `streak_before` INTEGER
            """)

        if db_version < 25:
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `mappref` TEXT
            """)

        if db_version < 26:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `map_pref_message` TEXT
            """)

        if db_version < 27:
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `requeue_random` INTEGER DEFAULT 1
            """)

        if db_version < 28:
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `pf_draws` INTEGER
            """)
            c.execute("""ALTER TABLE `channel_players`
            ADD COLUMN `draws` INTEGER
            """)

        if db_version < 29:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `blacklist_msg` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `blacklist_msg` TEXT
            """)

        if db_version < 30:
            c.execute("""ALTER TABLE `pickup_configs`
            ADD COLUMN `extension` TEXT
            """)
            c.execute("""ALTER TABLE `channels`
            ADD COLUMN `extension` TEXT
            """)

        c.execute("INSERT OR REPLACE INTO utility (variable, value) VALUES ('version', ?)", (str(version),))
        conn.commit()


def create_game_servers_table():
    c.execute("""CREATE TABLE `game_servers` 
        ( `game_server_id` INTEGER,
        `server_id` INTEGER,
        `ip` VARCHAR(30),
        `additional_info` BLOB,
        PRIMARY KEY(`game_server_id`) )""")


def create_member_info_table():
    c.execute("""CREATE TABLE `member_info` 
        (
        `user_id` INTEGER,
        `server_id` INTEGER,
        `nick` VARCHAR(25),
        `country` VARCHAR(2),
        PRIMARY KEY(`user_id`,`server_id`) )""")


def create_status_messages_table():
    c.execute("""CREATE TABLE `status_messages` 
            (
            `message_id` INTEGER,
            `channel_id` INTEGER,
            `channel_ids` BLOB,
            PRIMARY KEY(`message_id`) )""")


def create_tables():
    c.execute("""CREATE TABLE `utility`
        ( `variable` TEXT,
        `value` TEXT,
        PRIMARY KEY(`variable`) )""")

    c.execute("""CREATE TABLE `bans` 
        ( `channel_id` INTEGER,
        `user_id` INTEGER,
        `user_name` TEXT,
        `active` BLOB,
        `at` INTEGER,
        `duratation` INTEGER,
        `reason` TEXT,
        `author_name` TEXT,
        `unban_author_name` TEXT )""")

    c.execute("""CREATE TABLE `channel_players` 
        ( `channel_id` INTEGER,
        `user_id` INTEGER,
        `nick` TEXT,
        `rank` INTEGER,
        `wins` INTEGER,
        `draws` INTEGER,
        `loses` INTEGER,
        `streak` INTEGER,
        `is_seeded` BLOB,
        `phrase` TEXT,
        `panzer_rank` INTEGER,
        `pf_wins` INTEGER,
        `pf_draws` INTEGER,
        `pf_loses` INTEGER,
        `mappref` TEXT,
        
        PRIMARY KEY(`channel_id`, `user_id`) )""")

    c.execute("""CREATE TABLE `channels` 
        ( `server_id` INTEGER,
        `server_name` TEXT,
        `channel_id` INTEGER,
        `channel_name` TEXT,
        `premium` BOOL,
        `first_init` INTEGER,
        `admin_id` INTEGER,
        `admin_role` INTEGER,
        `moderator_role` INTEGER,
        `captains_role` INTEGER,
        `panzer_role` INTEGER,
        `noadd_role` INTEGER,
        `prefix` TEXT DEFAULT '!',
        `default_bantime` INTEGER DEFAULT 7200,
        `++_req_players` INTEGER DEFAULT 5,
        `startmsg` TEXT,
        `submsg` TEXT,
        `promotemsg` TEXT,
        `ip` TEXT,
        `password` TEXT,
        `maps` TEXT,
        `pick_captains` INTEGER,
        `team_emojis` TEXT,
        `team_names` TEXT,
        `pick_teams` TEXT DEFAULT 'no_teams',
        `pick_order` TEXT,
        `promotion_role` INTEGER,
        `promotion_delay` INTEGER DEFAULT 18000,
        `blacklist_role` INTEGER,
        `whitelist_role` INTEGER,
        `require_ready` INTEGER,
        `ranked` INTEGER,
        `ranked_multiplayer` INTEGER DEFAULT 32,
        `ranked_calibrate` INTEGER DEFAULT 1,
        `ranked_streaks` INTEGER DEFAULT 1,
        `initial_rating` INTEGER,
        `match_livetime` INTEGER,
        `global_expire` INTEGER,
        `start_pm_msg` TEXT DEFAULT '**%pickup_name%** pickup has been started @ %channel%.',
        `best_of` INTEGER DEFAULT 1,
        `party` INTEGER,
        `servers` TEXT,
        `hide_ranks` INTEGER,
        `public` INTEGER,
        `custom_ranks` TEXT,
        `requeue_random` INTEGER INTEGER DEFAULT 1,
        `extension` TEXT,
        PRIMARY KEY(`channel_id`) )""")

    c.execute("""CREATE TABLE `pickup_configs` 
        ( `channel_id` INTEGER,
        `pickup_name` TEXT,
        `maxplayers` INTEGER,
        `minplayers` INTEGER,
        `startmsg` TEXT,
        `start_pm_msg` TEXT,
        `submsg` TEXT,
        `promotemsg` TEXT,
        `ip` TEXT,
        `password` TEXT,
        `maps` TEXT,
        `pick_captains` INTEGER,
        `captains_role` INTEGER,
        `team_emojis` TEXT,
        `team_names` TEXT,
        `pick_teams` TEXT,
        `pick_order` TEXT,
        `promotion_role` INTEGER,
        `blacklist_role` INTEGER,
        `whitelist_role` INTEGER,
        `captain_role` INTEGER,
        `require_ready` INTEGER,
        `ranked` INTEGER,
        `allow_offline` INTEGER DEFAULT 0,
        `map_pick_order` TEXT,
        `best_of` INTEGER DEFAULT 1,
        `servers` TEXT,
        `map_probabilities` TEXT,
        `no_repeat_maps` TEXT,
        `map_pref_message` TEXT,
        `extension` TEXT,
        PRIMARY KEY(`channel_id`, `pickup_name`) )""")

    c.execute("""CREATE TABLE `pickups` 
        ( `pickup_id` INTEGER PRIMARY KEY,
        `channel_id` INTEGER,
        `pickup_name` TEXT,
        `at` INTEGER,
        `players` TEXT,
        `alpha_players` TEXT,
        `beta_players` TEXT,
        `is_ranked` BOOL,
        `winner_team` TEXT,
        `maps` TEXT,
        `panzers` TEXT)""")

    c.execute("""CREATE TABLE `player_pickups` 
        ( `pickup_id` INTEGER,
        `channel_id` INTEGER,
        `user_id` INTEGER,
        `user_name` TEXT,
        `pickup_name` TEXT,
        `at` INTEGER,
        `team` TEXT,
        `is_ranked` BOOL,
        `is_winner` BLOB,
        `rank_after` INTEGER,
        `rank_change` INTEGER,
        `is_lastpick` BLOB,
        `is_panzer` BLOB,
        `streak_before` INTEGER)""")

    c.execute("""CREATE TABLE `players` 
        ( `user_id` INTEGER,
        `default_expire` INTEGER,
        `disable_pm` BLOB,
        PRIMARY KEY(`user_id`) )""")

    c.execute("""CREATE TABLE `pickup_groups` 
        ( `channel_id` INTEGER,
        `group_name` TEXT,
        `pickup_names` TEXT,
        PRIMARY KEY(`channel_id`, `group_name`) )""")

    create_game_servers_table()
    create_member_info_table()
    create_status_messages_table()

    c.execute("""CREATE TABLE `nukem_quotes` ( `quote` TEXT )""")
    c.executemany("""INSERT INTO nukem_quotes ('quote') VALUES (?)""",
                  [["AAhhh... much better!"], ["Bitchin'!"], ["Come get some!"], ["Do, or do not, there is no try."],
                   ["Eat shit and die."], ["Get that crap outta here!"], ["Go ahead, make my day."],
                   ["Hail to the king, baby!"], ["Heh, heh, heh... what a mess!"], ["Holy cow!"], ["Holy shit!"],
                   ["I'm gonna get medieval on your asses!"], ["I'm gonna kick your ass, bitch!"],
                   ["Let God sort 'em out!"], ["Ooh, that's gotta hurt."], ["See you in Hell!"], ["Piece of Cake."],
                   ["Suck it down!"], ["Terminated!"], ["Your face, your ass - what's the difference?"],
                   ["Nobody fucks up our pickups... and lives!"], ["My boot, your face; the perfect couple."]])

    c.execute("INSERT INTO utility (variable, value) VALUES ('version', ?)", (str(version),))
    conn.commit()


def close():
    conn.commit()
    conn.close()
