from functools import wraps
from quart import request, abort


def local_request(f):
    @wraps(f)
    async def decorated_function(*args, **kwargs):
        if request.remote_addr != '127.0.0.1' \
                or hasattr(request.headers, 'X-Real-Ip'):  # nginx proxy request from outside
            abort(403)
        return await f(*args, **kwargs)
    return decorated_function
